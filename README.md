# Learning rust - tinymd

This repository was created by following the ['Rust TinyMD Tutorial'][1] by Jesse
Lawson. Check out the [original repo][2].


[1]: https://jesselawson.org/rust/getting-started-with-rust-by-building-a-tiny-markdown-compiler/
[2]: https://github.com/jesselawson/rust-tiny-markdown-compiler-tutorial

